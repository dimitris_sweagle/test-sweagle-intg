#/bin/sh
source $(dirname "$0")/sweagle.env

$sweagleScriptDir/uploadFileToSweagle.sh "$2,environments,dev,deployed" $1

awk -F'=' '{ print $1"=@@env."$1"@@" }' $1 > ./template.properties

$sweagleScriptDir/uploadFileToSweagle.sh "$2,environments,dev,socle-local-projet" ./template.properties
$sweagleScriptDir/uploadFileToSweagle.sh "$2,environments,int,socle-local-projet" ./template.properties
