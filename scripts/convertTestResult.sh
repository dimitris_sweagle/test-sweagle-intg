#!/usr/bin/env bash

CDS=$1
TEST_RESULT_PATH="./outputs/testResults"

for file in $(find $TEST_RESULT_PATH -name "testResult-validator-*.json"); do
  filename=$(basename -- "$file")
  echo "Converting $filename to Junit XML format"
  VALIDATOR="${filename%.json}"
  VALIDATOR="${VALIDATOR##testResult-validator-}"
  TEST_RESULT_JSON=$(cat "$file")
  TEST_RESULT_XML_FILE="$TEST_RESULT_PATH/testResult-validator-$VALIDATOR.xml"
  #TEST_RESULT_XML_FILE="./testResult-validator-$VALIDATOR.xml"
  if [[ "$TEST_RESULT_JSON" == *"result: true"* ]]; then
    TEST_RESULT=0
  else
    TEST_RESULT=1
  fi
  #TEST_DESCRIPTION=$(sed -n -e 's/^.*description: //p' $file)
  TEST_DESCRIPTION=$(echo "$TEST_RESULT_JSON" | awk 'BEGIN{found=0;}/description:/{if(!found){found=1;$0=substr($0,index($0, "description:")+13);}}/passed:/{if(found){found=2;$0=substr($0,0,index($0,"passed:")-3);}}{ if(found){print;if(found==2)found=0;}}')
  #TEST_DESCRIPTION=$(sed -n -e 's/^.*description: \(.*\)passed://p' $file)
  TEST_DESCRIPTION=${TEST_DESCRIPTION//##/}
  TEST_DESCRIPTION=${TEST_DESCRIPTION//\'/}
  TIMESTAMP=$(date "+%Y-%m-%dT%H:%M:%S")

  echo '<?xml version="1.0" encoding="UTF-8"?>' > "$TEST_RESULT_XML_FILE"
  echo "<testsuite name=\"sweagle\" tests=\"1\" failures=\"$TEST_RESULT\" hostname=\"testing.sweagle.com\" time=\"0.213\" timestamp=\"${TIMESTAMP}Z\">" >> "$TEST_RESULT_XML_FILE"
  echo "<testcase name=\"$VALIDATOR\" classname=\"$CDS\">" >> "$TEST_RESULT_XML_FILE"
  if [[ $TEST_RESULT -ne 0 ]]; then
    echo "<failure message=\"$TEST_DESCRIPTION\" type=\"ERROR\">$TEST_DESCRIPTION</failure>" >> "$TEST_RESULT_XML_FILE"
  fi
  echo '</testcase></testsuite>' >> "$TEST_RESULT_XML_FILE"

  # Display result for debugging
  #cat "$TEST_RESULT_XML_FILE"
done
