#/bin/sh
declare -A CONFIG

PROPERTY_FILE="$1"

function getProperty {
       PROP_KEY=$1
       PROP_VALUE=`cat $PROPERTY_FILE | grep "$PROP_KEY" | cut -d'=' -f2`
       echo $PROP_VALUE
    }

mkdir -p ./release/
echo $(getProperty "EQUIPMENT_ID") > ./release/instance.txt
