#!/usr/bin/env bash
source $(dirname "$0")/sweagle.env

$sweagleScriptDir/getConfigByMdsParserArgs.sh $1 returnIndexValue format=PROPS output=./instance.txt
INSTANCE=$(cat ./instance.txt)
INSTANCE=${INSTANCE//\"/}
$sweagleScriptDir/uploadFileToSweagle.sh "$2,Instances,F5,$INSTANCE" "$3"
