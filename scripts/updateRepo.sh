#!/usr/bin/env bash

sweagleScriptDir=$(dirname "$0")
source $sweagleScriptDir/sweagle.env

SERVICE=$1
NODE_PATH=$2
ENV=$3
CONFIG_FILE=$4

echo $sweagleScriptDir

if [ -f "./release/instance.txt" ]; then INSTANCE=$(cat ./release/instance.txt); fi
if [ -z "$INSTANCE" ]; then
  echo "Instance does not exists"
  # if INSTANCE is null, calculate it
  $sweagleScriptDir/getConfigByMdsParserArgs.sh $SERVICE returnValueforKey args=host mdsArgs=$NODE_PATH-$ENV format=PROPS output=./instance.txt
  INSTANCE=$(cat ./instance.txt)
  INSTANCE=${INSTANCE//\"/}
fi
$sweagleScriptDir/uploadFileToSweagle.sh "orange-dtsi,$NODE_PATH,$ENV,$INSTANCE" "$CONFIG_FILE"
